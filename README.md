# Adafruit RP2040-Feather
## Setup environment
1. Download and install rust via [`rustup`](https://rustup.rs) 
1. Download target toolchain `rustup target install thumbv6m-none-eabi`
1. Put the `rp2040` in [CMSIS-DAP mode](#cmsis-dap-mode)
1. Setup [`openocd`](#rpi-openocd)
1. Use [`gdb`](#gdb-setup) to load and debug the binary

### `CMSIS-DAP` mode 
[pico-debug bootloader reference](https://github.com/majbthrd/pico-debug/releases/download/v10.04/pico-debug-gimmecache.uf2)
1. Hold down BOOTSEL when plugging in device
1. Mount the Raspberry Pi to the file system
1. drag and drop the `bootloader/pico-debug-gimmecache.uf2` file onto the disk, top level directory
1. the disk will reboot into `cmsis-dap` mode which is needed for `openocd` 

### RPi `openocd`
[`openocd`](https://github.com/raspberrypi/openocd) is the translation layer for gdb to talk to. Needed to build from scratch due to missing cfgs
Most boards configs are on by default. The _interface_ configurations are located in `/usr/local/share/openocd/scripts/*`. The _target_ configurations are with raspberrypi's custom `openocd` repo: `./tcl/target/*` 
1. `./bootstrap`
1. `./configure --enable-ftdi --enable-sysfsgpio --enable-bcm2835gpio`
1. `make -j4`
1. `sudo make install`
1. With the current working directory being `openocd` run the following:
    ```
    sudo openocd -f interface/cmsis-dap.cfg -f target/rp2040-core0.cfg -c "transport select swd" -c "adapter speed 4000"
    ```
1. Leave the shell open as this is the translation layer

### `gdb` Setup
1. grab package `arm-none-eabi-gdb` 
1. then run the following within this repo:  
    ```
    arm-none-eabi-gdb -ex "target extended-remote :3333" target/thumbv6m-none-eabi/debug/rp2040-feather
    ```
