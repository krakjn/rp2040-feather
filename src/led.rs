use adafruit_feather_rp2040::hal::gpio;
use embedded_hal::digital::v2::OutputPin;

pub type LedPin = gpio::Pin<gpio::bank0::Gpio13, gpio::Output<gpio::PushPull>>;

/// will flash onboard LED the amount of times given in count with a delay of ms
pub fn flash_led(led_pin: &mut LedPin, delay: &mut cortex_m::delay::Delay, count: u8, ms: u32) {
    for _ in 0..count {
        led_pin.set_high().unwrap();
        delay.delay_ms(ms);
        led_pin.set_low().unwrap();
    }
}
