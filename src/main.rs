#![no_std]
#![no_main]

use adafruit_feather_rp2040::entry;
use core::fmt::Write;
use embedded_hal::digital::v2::OutputPin;
use panic_halt as _;

mod board;
mod display;
mod led;
mod mpu6050;
mod sensor;

#[entry]
fn main() -> ! {
    //? In Order to share an I2C bus with multiple devices
    //? https://github.com/rust-embedded/embedded-hal/issues/35

    let mut rp2040 = board::setup();
    let i2c_bus = shared_bus::BusManagerSimple::new(rp2040.i2c_dev);
    //? 16 char width
    let mut oled = display::new(i2c_bus.acquire_i2c());
    writeln!(oled, "Booting up...").unwrap();
    rp2040.delay.delay_ms(2000);
    let mut mpu = sensor::get_sensor(
        i2c_bus.acquire_i2c(),
        &mut rp2040.delay,
        &mut rp2040.led_pin,
    );

    rp2040.led_pin.set_high().unwrap();

    loop {
        rp2040.delay.delay_ms(500);
        oled.clear().unwrap();

        let roll_pitch = mpu.get_acc_angles().unwrap();
        writeln!(oled, "roll:   {:.4}", roll_pitch.x).unwrap();
        writeln!(oled, "pitch:  {:.4}", roll_pitch.y).unwrap();

        let temp = mpu.get_temp().unwrap();
        writeln!(oled, "temp C: {:.4}", temp).unwrap();

        //? scaled with sensitivity
        let gyro = mpu.get_gyro().unwrap();
        writeln!(oled, "gyro x: {:.4}", gyro.x).unwrap();
        writeln!(oled, "gyro y: {:.4}", gyro.y).unwrap();
        writeln!(oled, "gyro z: {:.4}", gyro.z).unwrap();

        //? get accelerometer data, scaled with sensitivity
        let acc = mpu.get_acc().unwrap();
        writeln!(oled, "accelX: {:.4}", acc.x).unwrap();
        writeln!(oled, "accelY: {:.4}", acc.y).unwrap();
    }
}
