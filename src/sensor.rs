use crate::{
    led::{flash_led, LedPin},
    mpu6050::{Mpu6050, Mpu6050Error},
};
use adafruit_feather_rp2040::hal::i2c;
use cortex_m::delay::Delay;
use embedded_hal::blocking::i2c::{Write, WriteRead};

/// Retrieves sensor on the I2C bus connected to the rp2040-feather
/// Upon error this function will flash the onboard LED to indicate
/// which error was caught.
pub fn get_sensor<I>(i2c: I, delay: &mut Delay, led_pin: &mut LedPin) -> Mpu6050<I>
where
    I: Write<Error = i2c::Error> + WriteRead<Error = i2c::Error>,
{
    let mut mpu = Mpu6050::new(i2c);
    if let Err(mpu_error) = mpu.init(delay) {
        match mpu_error {
            Mpu6050Error::I2c(bus_error) => {
                match bus_error {
                    i2c::Error::Abort(val) => {
                        if val & 1 << 12 != 0 {
                            // Arbitration Lost
                            flash_led(led_pin, delay, 8, 1000);
                        } else if val & 1 << 7 != 0 {
                            // SBYTE_ACKDET
                            flash_led(led_pin, delay, 7, 1000);
                        } else if val & 1 << 6 != 0 {
                            // HS_ACKDET
                            flash_led(led_pin, delay, 6, 1000);
                        } else if val & 1 << 4 != 0 {
                            // GCALL_NOACK
                            flash_led(led_pin, delay, 5, 1000);
                        } else if val & 1 << 3 != 0 {
                            // TXDATA_NOACK
                            flash_led(led_pin, delay, 4, 1000);
                        } else if val & 1 << 2 != 0 {
                            // 10ADDR2_NOACK
                            flash_led(led_pin, delay, 3, 1000);
                        } else if val & 1 << 1 != 0 {
                            // 10ADDR1_NOACK
                            flash_led(led_pin, delay, 2, 1000);
                        } else if val & 1 << 0 != 0 {
                            // 7B_ADDR_NOACK
                            flash_led(led_pin, delay, 1, 1000);
                        }
                    }
                    // User passed in a read buffer that was 0 length
                    i2c::Error::InvalidReadBufferLength => {
                        flash_led(led_pin, delay, 2, 500);
                    }
                    // User passed in a write buffer that was 0 length
                    i2c::Error::InvalidWriteBufferLength => {
                        flash_led(led_pin, delay, 3, 500);
                    }
                    // Target i2c address is out of range
                    i2c::Error::AddressOutOfRange(_) => {
                        flash_led(led_pin, delay, 4, 500);
                    }
                    // Target i2c address is reserved
                    i2c::Error::AddressReserved(_) => {
                        flash_led(led_pin, delay, 5, 500);
                    }
                    _ => (),
                }
            }
            Mpu6050Error::InvalidChipId(chip_id) => panic!("Invalid chip id: {}", chip_id),
        }
    }
    return mpu;
}
