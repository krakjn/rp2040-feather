use embedded_hal::blocking::i2c;
use ssd1306::{mode::TerminalMode, prelude::*, I2CDisplayInterface, Ssd1306};

type Display<I> = Ssd1306<I2CInterface<I>, DisplaySize128x64, TerminalMode>;

pub fn new<I>(i2c: I) -> Display<I>
where
    I: i2c::Write,
{
    let interface = I2CDisplayInterface::new_alternate_address(i2c);
    let mut display =
        Ssd1306::new(interface, DisplaySize128x64, DisplayRotation::Rotate0).into_terminal_mode();
    display.init().unwrap();
    display.clear().unwrap();
    display
}
