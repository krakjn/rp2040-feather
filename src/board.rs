use crate::led::LedPin;
use adafruit_feather_rp2040::{
    hal::{
        clocks::{init_clocks_and_plls, Clock},
        gpio, i2c, pac,
        watchdog::Watchdog,
        Sio, I2C,
    },
    pac::I2C1,
    XOSC_CRYSTAL_FREQ,
};
use embedded_time::rate::*;
use rp2040_hal::gpio::bank0::*;

type I2CDev = I2C<
    I2C1,
    (
        gpio::Pin<Gpio2, gpio::Function<gpio::I2C>>,
        gpio::Pin<Gpio3, gpio::Function<gpio::I2C>>,
    ),
>;

pub struct Board {
    pub delay: cortex_m::delay::Delay,
    pub led_pin: LedPin,
    pub i2c_dev: I2CDev,
}

pub fn setup() -> Board {
    let mut peripherals = pac::Peripherals::take().unwrap();
    let core_peripherals = pac::CorePeripherals::take().unwrap();

    let mut watchdog = Watchdog::new(peripherals.WATCHDOG);

    let clocks = init_clocks_and_plls(
        XOSC_CRYSTAL_FREQ,
        peripherals.XOSC,
        peripherals.CLOCKS,
        peripherals.PLL_SYS,
        peripherals.PLL_USB,
        &mut peripherals.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let delay =
        cortex_m::delay::Delay::new(core_peripherals.SYST, clocks.system_clock.freq().integer());

    let sio = Sio::new(peripherals.SIO);
    let pins = gpio::Pins::new(
        peripherals.IO_BANK0,
        peripherals.PADS_BANK0,
        sio.gpio_bank0,
        &mut peripherals.RESETS,
    );
    let led_pin = pins.gpio13.into_push_pull_output();

    let i2c_dev = i2c::I2C::i2c1(
        peripherals.I2C1,
        pins.gpio2.into_mode(), // sda
        pins.gpio3.into_mode(), // scl
        400.kHz(),
        &mut peripherals.RESETS,
        125_000_000.Hz(),
    );

    Board {
        delay,
        led_pin,
        i2c_dev,
    }
}
